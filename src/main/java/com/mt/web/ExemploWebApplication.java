package com.mt.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExemploWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExemploWebApplication.class, args);
	}
}
