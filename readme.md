
# Usando Gitlab AutoDevOps


## Quando salvo um arquivo quero que as mudanças sejam aplicadas (Live Reload/Restart)
  - Agregar o Spring Boot Dev Tools no pom.xml projeto
  	- https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-devtools.html
    - https://gitlab.com/distributedarchitecturecourse/exemplo-ms-1/tree/v1.0

- Rodar na maquina local sem Docker nem Kubernets com as URL dos serviços marretadas (ServiceDiscovery)

- Poder fazer o build das imagens docker e os arquivos de configuração para Kubernetes
	- http://maven.fabric8.io/

- Como Netflix resolve alguns dos problemas do 12Factors
	- https://cloud.spring.io/spring-cloud-netflix/multi/multi_pr01.html

- Integrando alguns dos serviços do Netflix kubernetes

- Preparando os Serviços para Rodar em Kubernetes
  - http://blog.christianposta.com/microservices/spring-boot-microservice-development-on-kubernetes-the-easy-way/
  
  - Adicionando o spring-cloud-starter-kubernetes
  	- https://github.com/fabric8io/spring-cloud-kubernetes
  - Serviçe Discovery com Kubernetes
  
- Adicionando clientes com Feign + H

- Fazer Integração continua usando Jenkins

- Fazar Imagens Docker com dockerfile-maven
	- https://github.com/spotify/dockerfile-maven
	
- Fazer Imagens Docker e Arquivos para instalar em Kubernetes
	- http://maven.fabric8.io/

- Fazer o CI no GitLab com Auto DevOps
	- https://docs.gitlab.com/ee/topics/autodevops/

- Fazer o CI no GitLab Fazendo CD em Kubernetes
	- https://about.gitlab.com/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/


- 

- Fazer deploy em Kubernetes
	- https://itnext.io/migrating-a-spring-boot-service-to-kubernetes-in-5-steps-7c1702da81b6
	
- Fazer um cluster de Kubernetes
  - http://45.55.39.155.nip.io:30000
	
- Fazer CI/CD com Fabric8
	- http://fabric8.io/guide/index.html
	- https://github.com/fabric8io/fabric8-platform/blob/master/INSTALL.md
	

- Poder fazer o deploy da app como um Chart de Helm
	- 
	- https://docs.bitnami.com/kubernetes/how-to/deploy-java-application-kubernetes-helm/
	

- Poder ter alguns serviços integrados atraves de Eventos (MessageQueue ou Kafka)
  - Notificação por Email da Compra

- Poder fazer uma query em tempo real usando os eventos que a App gera (KSQL)

- Poder Criar um Movo Cluster do Zero com StackPoint e Instalar o App como um Chart
	- https://stackpoint.io/clusters/list
- Poder Usar Istio com o App deles

- Poder Usar Prometheus com o App deles
- Poder Usar Elasticsearch + fluend + kibana
- Poder Usar um Service Mesh - Instio na App
	- https://www.slideshare.net/ceposta/microservices-and-integration-whats-next-with-istio-service-mesh
	- https://istio.io/
	- https://github.com/redhat-developer-demos/istio-tutorial
	- https://learn.openshift.com/servicemesh


# Clusters Kubernetes + Helm
- 
- https://kubeapps.com/
- https://hub.kubeapps.com/

